<?php

namespace App\Http\Controllers\Approvals;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PendingController extends Controller
{
    //this displays the pending screen
    public function pendingApprovals(){
        return view('pages.approvals.pending');
    }

    public function approvals_pending_transfer_details()
    {
        return view('pages.approvals.pending_transfer_details');
    }

}
