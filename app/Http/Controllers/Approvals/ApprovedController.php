<?php

namespace App\Http\Controllers\Approvals;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApprovedController extends Controller
{
    //method to return the approved transactions screen
    public function approvedApprovals(){
        return view('pages.approvals.approved');
    }
}
