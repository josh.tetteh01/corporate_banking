<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="h-100" data-simplebar>

        <!-- User box -->
        <div class="user-box text-center">
            <img src="../assets/images/users/user-1.jpg" alt="user-img" title="Mat Helme"
                class="rounded-circle avatar-md">
            <div class="dropdown">
                <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block"
                    data-toggle="dropdown">Geneva Kennedy</a>
                <div class="dropdown-menu user-pro-dropdown">

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-user mr-1"></i>
                        <span>My Account</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-settings mr-1"></i>
                        <span>Settings</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-lock mr-1"></i>
                        <span>Lock Screen</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-log-out mr-1"></i>
                        <span>Logout</span>
                    </a>

                </div>
            </div>
            <p class="text-muted">Admin Head</p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul id="side-menu">

                {{--  <li class="menu-title">Navigation</li>  --}}

                <li>
                    <a href="{{ url('home') }}" data-toggle="collapse">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        {{--  <span class="badge badge-info badge-pill float-right">4</span>
                          --}}
                        <span> Home</span>
                    </a>
                </li>

                <li>
                    <a href="#sidebarTransfer" data-toggle="collapse">
                        <i class="mdi mdi-cart-outline"></i>
                        <span> Transfer </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarTransfer">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ url('own-bank-account-transfer') }}">Own Account</a>
                            </li>
                            <li>
                                <a href="{{ url('same-bank-account-transfer') }}">Same Bank</a>
                            </li>
                            <li>
                                <a href="{{ url('other-local-transfer') }}">Other Local Transfer</a>
                            </li>
                            <li>
                                <a href="#">International Transfer</a>
                            </li>
                            <li>
                                <a href="{{ url('add-beneficiary') }}">Add Beneficiary</a>
                            </li>

{{--
                            <li>
                                <a href="ecommerce-customers.html">Customers</a>
                            </li>
                            <li>
                                <a href="ecommerce-orders.html">Orders</a>
                            </li>
                            <li>
                                <a href="ecommerce-order-detail.html">Order Detail</a>
                            </li>
                            <li>
                                <a href="ecommerce-sellers.html">Sellers</a>
                            </li>
                            <li>
                                <a href="ecommerce-cart.html">Shopping Cart</a>
                            </li>
                            <li>
                                <a href="ecommerce-checkout.html">Checkout</a>
                            </li>
                              --}}
                        </ul>
                    </div>
                </li>


                <li>
                    <a href="#sidebarCrm" data-toggle="collapse">
                        <i class="mdi mdi-account-multiple-outline"></i>
                        <span>Payments </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarCrm">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ url('saved-beneficiary') }}">Pay to Saved Beneficiary</a>
                            </li>
                            <li>
                                <a href="{{ url('one-time-payment') }}">One Time Payment</a>
                            </li>
                            <li>
                                <a href="{{ url('payment-add-beneficiary') }}">Add Beneficiary</a>
                            </li>

{{--
                            <li>
                                <a href="crm-leads.html">Leads</a>
                            </li>
                            <li>
                                <a href="crm-customers.html">Customers</a>
                            </li>
                              --}}
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="#sidebarEmail" data-toggle="collapse">
                        <i class="mdi mdi-email-multiple-outline"></i>
                        <span> Account Services </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarEmail">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ url('/openAccount') }}">Open Account</a>
                            </li>
                            <li>
                                <a href="#">Cheque Account Request</a>
                            </li>
                            <li>
                                <a href="#">Request ATM</a>
                            </li>
                        </ul>
                    </div>
                </li>
{{--
                <li>
                    <a href="apps-social-feed.html">
                        <span class="badge badge-primary float-right">Hot</span>
                        <i class="mdi mdi-rss"></i>
                        <span> Social Feed </span>
                    </a>
                </li>  --}}

                {{--  <li>
                    <a href="apps-companies.html">
                        <i class="mdi mdi-domain"></i>
                        <span> Companies </span>
                    </a>
                </li>  --}}

                <li>
                    <a href="#sidebarLoans" data-toggle="collapse">
                        <i class="mdi mdi-briefcase-check-outline"></i>
                        <span> Loans </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarLoans">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ url('loan') }}">Loan Quotation</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="#sidebarTasks" data-toggle="collapse">
                        <i class="mdi mdi-clipboard-multiple-outline"></i>
                        <span> Cards </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarTasks">
                        <ul class="nav-second-level">
                            <li>
                                <a href="task-list.html">List</a>
                            </li>
                            <li>
                                <a href="task-details.html">Details</a>
                            </li>
                            <li>
                                <a href="task-kanban-board.html">Kanban Board</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="#sidebarContacts" data-toggle="collapse">
                        <i class="mdi mdi-book-account-outline"></i>
                        <span> Budgeting </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarContacts">
                        <ul class="nav-second-level">
                            <li>
                                <a href="contacts-list.html">Members List</a>
                            </li>
                            <li>
                                <a href="contacts-profile.html">Profile</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="#sidebarApprovals" data-toggle="collapse">
                        <i class="mdi mdi-cart-outline"></i>
                        <span> Approvals </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarApprovals">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ url('pending-approvals') }}">Pending</a>
                            </li>
                            <li>
                                <a href="{{ url('rejected-approvals') }}">Rejected</a>
                            </li>
                            <li>
                                <a href="{{ url('approved-approvals') }}">Approved</a>
                            </li>
                        </ul>
                    </div>
                </li>


                <li>
                    <a href="#sidebarTickets" data-toggle="collapse">
                        <i class="mdi mdi-lifebuoy"></i>
                        <span> Branch Locator </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarTickets">
                        <ul class="nav-second-level">
                            <li>
                                <a href="tickets-list.html">List</a>
                            </li>
                            <li>
                                <a href="tickets-detail.html">Detail</a>
                            </li>
                        </ul>
                    </div>
                </li>


{{--
                <li>
                    <a href="apps-file-manager.html">
                        <i class="mdi mdi-folder-star-outline"></i>
                        <span> File Manager </span>
                    </a>
                </li>  --}}
{{--
                <li class="menu-title mt-2">Custom</li>  --}}

                <li>
                    <a href="#sidebarAuth" data-toggle="collapse">
                        <i class="mdi mdi-account-circle-outline"></i>
                        <span> Chatbot/WhatsApp Banking  </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarAuth">
                        <ul class="nav-second-level">
                            <li>
                                <a href="auth-login.html">Log In</a>
                            </li>
                            <li>
                                <a href="auth-login-2.html">Log In 2</a>
                            </li>
                            <li>
                                <a href="auth-register.html">Register</a>
                            </li>
                            <li>
                                <a href="auth-register-2.html">Register 2</a>
                            </li>
                            <li>
                                <a href="auth-signin-signup.html">Signin - Signup</a>
                            </li>
                            <li>
                                <a href="auth-signin-signup-2.html">Signin - Signup 2</a>
                            </li>
                            <li>
                                <a href="auth-recoverpw.html">Recover Password</a>
                            </li>
                            <li>
                                <a href="auth-recoverpw-2.html">Recover Password 2</a>
                            </li>
                            <li>
                                <a href="auth-lock-screen.html">Lock Screen</a>
                            </li>
                            <li>
                                <a href="auth-lock-screen-2.html">Lock Screen 2</a>
                            </li>
                            <li>
                                <a href="auth-logout.html">Logout</a>
                            </li>
                            <li>
                                <a href="auth-logout-2.html">Logout 2</a>
                            </li>
                            <li>
                                <a href="auth-confirm-mail.html">Confirm Mail</a>
                            </li>
                            <li>
                                <a href="auth-confirm-mail-2.html">Confirm Mail 2</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="#sidebarExpages" data-toggle="collapse">
                        <i class="mdi mdi-text-box-multiple-outline"></i>
                        <span> Settings </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarExpages">
                        <ul class="nav-second-level">
                            <li>
                                <a href="#">Change Password</a>
                            </li>
                            <li>
                                <a href="#">Change Pin</a>
                            </li>
                            <li>
                                <a href="#">Forget Pin</a>
                            </li>
                        </ul>
                    </div>
                </li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
