<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class paymentController extends Controller
{
    //method to return the add_beneficiary page
    public function addBeneficiary(){
        return view("pages.payments.payment_add_beneficiary");
    }

    //method to return the mobile_money_beneficiary page
    public function mobileMoneyBeneficiary(){
        return view("pages.payments.mobile_money_beneficiary");
    }

    //method to return the utility_payment page
    public function utilityPayment(){
        return view("pages.payments.utility_payment_beneficiary");
    }

    //method to return the saved_beneficiary page
    public function savedBeneficiary(){
        return view("pages.payments.saved_beneficiary");
    }

    //method to return the one_time_payment page
    public function one_time_payment(){
        return view("pages.payments.one_time");
    }
}

