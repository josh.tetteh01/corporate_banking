<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class transferController extends Controller
{
    //
    public function transfer()
    {
        return view('pages.transfer.transfer');
    }

    //function to return ownAccountTransfer screen
    public function own_bank_account_transfer(){
        return view('pages.transfer.own_account');
    }

    //function to return ownAccountTransfer screen
    public function same_bank_account_transfer(){
        return view('pages.transfer.same_bank');
    }

    //functioin to return other_local_transfer screen
    public function other_local_transfer(){
        return view('pages.transfer.other_local_bank');
    }

    //function to return add_beneficiary screen
    public function add_beneficiary(){
        return view('pages.transfer.add_beneficiary');
    }
}
