<?php

namespace App\Http\Controllers\Approvals;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RejectedController extends Controller
{
    //this displays the rejected screen
    public function rejectedApprovals(){
        return view('pages.approvals.rejected');
    }
}
